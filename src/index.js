import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { Provider } from "react-redux";
import configureStore from "./redux/persist/configureStore";
import { PersistGate } from "redux-persist/integration/react";
import Firebase, { FirebaseContext } from './config/firebase'

/*
-provider
  -store
    -reducer
      -action
*/
const { store, persistor } = configureStore();

if(store === persistor) {
  console.log("done");
}


ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <FirebaseContext.Provider value={new Firebase()}>
        <PersistGate loading={null} persistor={persistor}>
          <App />
        </PersistGate>
      </FirebaseContext.Provider>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
