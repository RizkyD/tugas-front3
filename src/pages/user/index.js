import React, { Component } from "react";
import "./index.css";
import { connect } from "react-redux";
import { addCart, deleteCart } from "../../redux/action/User";

class User extends Component {
  addcart = (id) => {
    let arr = this.props.item.filter((item) => item.id === id);
    let did = this.props.auth.id;
    const index = this.props.cart.find((value) => {
      return value.id === id && value.user === did;
    });
    if (index === undefined) {
      this.props.addCart(
        did,
        arr[0].id,
        arr[0].barang,
        arr[0].harga,
        arr[0].photo
      );
    } else {
      alert("Data udah di cart");
    }

    this.forceUpdate();
  };

  deleteCart = (id) => {
    const findIndex = this.props.cart
      .filter((data) => data.user === this.props.auth.id)
      .findIndex((value) => {
        return value.id === id;
      });
    this.props.deleteCart(findIndex);
    this.forceUpdate();
  };

  render() {
    const list = this.props.item.map((item) => (
      <tr key={item.id}>
        <td>{item.barang}</td>
        <td>{item.harga}</td>
        <td>
          <img className="photo" alt={item.photo} src={item.photo} />
        </td>
        <td>
          <button onClick={() => this.addcart(item.id)}>add cart</button>
        </td>
      </tr>
    ));
    const cart = this.props.cart
      .filter((data) => data.user === this.props.auth.id)
      .map((item) => (
        <tr key={item.id}>
          <td>{item.barang}</td>
          <td>{item.harga}</td>
          <td>
            <img className="photo" alt={item.photo} src={item.photo} />
          </td>
          <td>
            <button onClick={() => this.deleteCart(item.id)}>
              delete cart
            </button>
          </td>
        </tr>
      ));
    return (
      <div className="exceed">
        <div>
          <h3>data barang</h3>
          <table>
            <thead>
              <tr>
                <th>Barang</th>
                <th>harga</th>
                <th>photo</th>
                <th></th>
              </tr>
            </thead>
            <tbody>{list}</tbody>
          </table>
        </div>
        <div>
          <h3>data Cart</h3>
          <table>
            <thead>
              <tr>
                <th>Barang</th>
                <th>harga</th>
                <th>photo</th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody>{cart}</tbody>
          </table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    item: state.item,
    auth: state.auth,
    cart: state.cart,
  };
};

const mapDispatchtoProps = (dispatch) => ({
  addCart: (user, id, barang, harga, photo) =>
    dispatch(addCart(user, id, barang, harga, photo)),
  deleteCart: (index) => dispatch(deleteCart(index)),
});

export default connect(mapStateToProps, mapDispatchtoProps)(User);
