import React, { Component } from "react";
import "./index.css";
import Admin from "../admin";
import Pedagang from "../pedagang";
import User from "../user";
import Navbar from "../component/navbar";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { connect } from "react-redux";

class Main extends Component {
  render() {
    const admin =
      this.props.profile.role === "admin" ? <Redirect to="/admin" /> : "";
    const pedagang =
      this.props.profile.role === "pedagang" ? <Redirect to="/pedagang" /> : "";
    const user =
      this.props.profile.role === "user" ? <Redirect to="/user" /> : "";
    return (
      <Router>
        <Navbar />
        {admin}
        {pedagang}
        {user}
        <Switch>
          <Route path="/admin">
            <Admin />
          </Route>
          <Route path="/pedagang">
            <Pedagang
            />
          </Route>
          <Route path="/user">
            <User />
          </Route>
        </Switch>
      </Router>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profile: {
      username: state.auth.username,
      password: state.auth.password,
      role: state.auth.role,
      status: state.auth.status,
    },
  };
};

export default connect(mapStateToProps)(Main);
