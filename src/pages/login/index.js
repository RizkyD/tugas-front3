import React, { Component } from "react";
import { connect } from "react-redux";
import "./index.css";
import { login } from "../../redux/action/Auth";
import {FirebaseContext} from '../../config/firebase'

class LoginForm extends Component {
  login = (e) => {
    e.preventDefault();

    const username = e.target.username.value;
    const password = e.target.password.value;

    const check = this.props.database.filter(
      (account) =>
        account.username === username && account.password === password
    );
    if (check.length === 1) {
      this.props.login(check[0].id, username, password, check[0].role);
    } else {
      alert("Login Gagal!");
    }
  };

  check = () => {
    return () => <h1>firebase</h1>
  }
  

  render() {
    return (
      <>
      <FirebaseContext.Consumer>
        {this.check()}
      </FirebaseContext.Consumer>
      <div className="main">
        <p className="sign" align="center">
          Login
        </p>
        <form className="form" onSubmit={this.login}>
          <input
            className="username"
            name="username"
            type="text"
            align="center"
            placeholder="Username"
          ></input>
          <input
            className="password"
            name="password"
            type="password"
            align="center"
            placeholder="Password"
          ></input>
          <button type="submit" className="submit" align="center">
            Sign in
          </button>
        </form>
      </div>
      </>
    );
  }
}

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = { }
  }
  render() {
    return (
      <FirebaseContext.Consumer>
          { (firebase) => <LoginForm {...this.props} firebase={firebase} />}
      </FirebaseContext.Consumer>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    profile: {
      username: state.auth.username,
      password: state.auth.password,
      role: state.auth.role,
      status: state.auth.status,
    },
    database: state.database,
  };
};

const mapDispatchtoProps = (dispatch) => ({
  login: (id, username, password, role) =>
    dispatch(login(id, username, password, role)),
});

export default connect(mapStateToProps, mapDispatchtoProps)(Login);
