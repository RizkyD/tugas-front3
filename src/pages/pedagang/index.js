import React, { Component } from "react";
import "./index.css";
import UpdateBarang from "../component/updateBarang";
import { connect } from "react-redux";
import { createItem, deleteItem } from "../../redux/action/Pedagang";

class Pedagang extends Component {
  constructor(props) {
    super(props);
    this.state = {
      update: false,
      id: null,
    };
  }

  create = (e) => {
    e.preventDefault();

    const barang = e.target.barang.value;
    const harga = e.target.harga.value;
    const photo = e.target.photo.value;

    this.props.createItem(barang, harga, photo, this.props.auth.id);
    this.forceUpdate();
  };

  delete = (barang) => {
    this.props.deleteItem(barang);
    this.forceUpdate();
  };

  update = (bool, id) => {
    this.setState({
      update: bool,
      id: id,
    });
  };

  render() {
    const list = this.props.item
      .filter((barang) => barang.pedagang === this.props.auth.id)
      .map((item) => (
        <tr key={item.id}>
          <td>{item.barang}</td>
          <td>{item.harga}</td>
          <td>
            <img className="photo" alt={item.photo} src={item.photo} />
          </td>
          <td>
            <button onClick={() => this.update(true, item.id)}>edit</button>
          </td>
          <td>
            <button onClick={() => this.delete(item.id)}>Delete</button>
          </td>
        </tr>
      ));
    const update =
      this.state.update === true ? (
        <UpdateBarang id={this.state.id} status={this.update} />
      ) : (
        ""
      );
    return (
      <div>
        <table>
          <thead>
            <tr>
              <th>Barang</th>
              <th>harga</th>
              <th>photo</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>{list}</tbody>
        </table>

        <br />
        <div>Tambah Barang {}</div>
        <form onSubmit={this.create}>
          <input
            className="barange"
            name="barang"
            type="text"
            align="center"
            placeholder="Nama Barang"
          ></input>
          <input
            className="hargae"
            name="harga"
            type="number"
            align="center"
            placeholder="harga"
          ></input>
          <input
            className="photoe"
            name="photo"
            type="text"
            align="center"
            placeholder="photo/url"
          ></input>
          <button type="submit" className="submite" align="center">
            tambah Barang
          </button>
        </form>
        <br />
        <br />
        {update}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    item: state.item,
    auth: state.auth,
  };
};
const mapDispatchtoProps = (dispatch) => ({
  createItem: (barang, harga, photo, pedagang) =>
    dispatch(createItem(barang, harga, photo, pedagang)),
  deleteItem: (id) => dispatch(deleteItem(id)),
});

export default connect(mapStateToProps, mapDispatchtoProps)(Pedagang);
