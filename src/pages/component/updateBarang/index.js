import React, { Component } from "react";
import "./index.css";
import { connect } from "react-redux";
import { updateItem } from "../../../redux/action/Pedagang";

class UpdateBarang extends Component {
  update = (e) => {
    e.preventDefault();

    const uBarang = e.target.updateBarang.value;
    const uHarga = e.target.updateHarga.value;
    const uPhoto = e.target.updatePhoto.value;

    this.props.updateItem(
      this.props.id,
      uBarang,
      uHarga,
      uPhoto,
      this.props.auth.id
    );
    this.props.status(false);
  };

  render() {
    return (
      <div>
        <br />
        <div>Update Barang</div>
        <form onSubmit={this.update}>
          <input
            name="updateBarang"
            type="text"
            align="center"
            placeholder="Barang"
          ></input>
          <input
            name="updateHarga"
            type="number"
            align="center"
            placeholder="Harga"
          ></input>
          <input
            name="updatePhoto"
            type="text"
            align="center"
            placeholder="Barang"
          ></input>
          <button type="submit" align="center">
            update
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    database: state.database,
    auth: state.auth,
  };
};

const mapDispatchtoProps = (dispatch) => ({
  updateItem: (id, barang, harga, photo, pedagang) =>
    dispatch(updateItem(id, barang, harga, photo, pedagang)),
});

export default connect(mapStateToProps, mapDispatchtoProps)(UpdateBarang);
