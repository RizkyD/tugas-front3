import React, { Component } from "react";
import "./index.css";
import { connect } from "react-redux";
import { update } from "../../../redux/action/Admin";

class Update extends Component {
  update = (e) => {
    e.preventDefault();

    const uUsername = e.target.updateUsername.value;
    const uPassword = e.target.updatePassword.value;
    const uRole = e.target.updateRole.value;

    this.props.update(this.props.id, uUsername, uPassword, uRole);
    this.props.status(false);
  };

  render() {
    return (
      <div>
        <br />
        <div>Update data</div>
        <form onSubmit={this.update}>
          <input
            name="updateUsername"
            type="text"
            align="center"
            placeholder="Username"
          ></input>
          <input
            name="updatePassword"
            type="password"
            align="center"
            placeholder="Password"
          ></input>
          <select name="updateRole">
            <option value="pedagang">Pedagang</option>
            <option value="user">User</option>
          </select>
          <button type="submit" align="center">
            update
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    database: state.database,
  };
};

const mapDispatchtoProps = (dispatch) => ({
  update: (id, username, password, role) =>
    dispatch(update(id, username, password, role)),
});

export default connect(mapStateToProps, mapDispatchtoProps)(Update);
