import React, { Component } from "react";
import "./index.css";
import { connect } from "react-redux";
import { logout } from "../../../redux/action/Auth";

class Navbar extends Component {
  logout = () => {
    this.props.logout();
  };

  render() {
    return (
      <div>
        <nav>
          <a href="!#">Home</a>
          <a href="!#" onClick={this.logout}>
            Logout
          </a>
          <div className="animation start-home"></div>
        </nav>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profile: {
      username: state.auth.username,
      password: state.auth.password,
      role: state.auth.role,
      status: state.auth.status,
    },
  };
};

const mapDispatchtoProps = (dispatch) => ({
  logout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchtoProps)(Navbar);
