import React, { Component } from "react";
import "./index.css";
import Update from "../component/update";
import { connect } from "react-redux";
import { create, delet } from "../../redux/action/Admin";

class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      update: false,
      id: null,
    };
  }

  create = (e) => {
    e.preventDefault();

    const username = e.target.username.value;
    const password = e.target.password.value;
    const role = e.target.role.value;

    this.props.create(username, password, role);
    this.forceUpdate();
  };

  delete = (id) => {
    this.props.delet(id);
    this.forceUpdate();
  };

  update = (bool, id) => {
    this.setState({
      update: bool,
      id: id,
    });
  };

  render() {
    const list = this.props.database
      .filter((account) => account.role !== "admin")
      .map((acc) => (
        <tr key={acc.id}>
          <td>{acc.username}</td>
          <td>{acc.password}</td>
          <td>{acc.role}</td>
          <td>
            <button onClick={() => this.update(true, acc.id)}>edit</button>
          </td>
          <td>
            <button onClick={() => this.delete(acc.id)}>Delete</button>
          </td>
        </tr>
      ));
    const update =
      this.state.update === true ? (
        <Update id={this.state.id} status={this.update} />
      ) : (
        ""
      );
    return (
      <div>
        <table>
          <thead>
            <tr>
              <th>Username</th>
              <th>Password</th>
              <th>Role</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>{list}</tbody>
        </table>

        <br />
        <div>Tambah data {}</div>
        <form onSubmit={this.create}>
          <input
            className="usernamee"
            name="username"
            type="text"
            align="center"
            placeholder="Username"
          ></input>
          <input
            className="passworde"
            name="password"
            type="password"
            align="center"
            placeholder="Password"
          ></input>
          <select name="role">
            <option value="pedagang">Pedagang</option>
            <option value="user">User</option>
          </select>
          <button type="submit" className="submite" align="center">
            tambah Akun
          </button>
        </form>
        <br />
        <br />
        {update}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    database: state.database,
  };
};

const mapDispatchtoProps = (dispatch) => ({
  create: (username, password, role) =>
    dispatch(create(username, password, role)),
  delet: (id) => dispatch(delet(id)),
});

export default connect(mapStateToProps, mapDispatchtoProps)(Admin);
