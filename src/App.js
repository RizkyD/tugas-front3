import React, { Component } from "react";
import { connect } from "react-redux";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import Login from "./pages/login";
import Main from "./pages/main";

class App extends Component {

  reset = () => {
    window.addEventListener(
      "storage",
      function (e) {
        localStorage.setItem("persist:root", e.oldValue);
      },
      false
    );
  };

  render() {
    this.reset();
    const loginOrMain =
      this.props.profile.status === false ? (
        <Redirect to="/login" />
      ) : (
        <Redirect to="/" />
      );
    return (
      <Router>
        {loginOrMain}
        <Switch>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/">
            <Main />
          </Route>
        </Switch>
      </Router>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profile: {
      username: state.auth.username,
      password: state.auth.password,
      role: state.auth.role,
      status: state.auth.status,
    },
  };
};

export default connect(mapStateToProps)(App);
