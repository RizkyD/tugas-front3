const initialState = {
  id: null,
  username: null,
  password: null,
  role: null,
  status: false,
};

const auth = (state = initialState, action) => {
  switch (action.type) {
    case "login":
      return {
        id: action.id,
        username: action.username,
        password: action.password,
        role: action.role,
        status: true,
      };
    case "logout":
      return {
        id: null,
        username: null,
        password: null,
        role: null,
        status: false,
      };
    default:
      return state;
  }
};

export default auth;
