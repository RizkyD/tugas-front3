const initialState = [];

const cart = (state = initialState, action) => {
  switch (action.type) {
    case "addCart":
      let temp = [
        {
          user: action.user,
          id: action.id,
          barang: action.barang,
          harga: action.harga,
          photo: action.photo,
        },
      ];
      const addCart = temp;

      return (state = [...state, ...addCart]);
    case "deleteCart":
      let index = state;
      index.splice(action.index, 1);
      return (state = [...index]);
    default:
      return state;
  }
};

export default cart;
