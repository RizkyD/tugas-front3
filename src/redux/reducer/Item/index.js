const initialState = [
  {
    id: 1,
    barang: "hp android",
    harga: 123321,
    photo: "andro1.jpg",
    pedagang: 2,
  },
];

const item = (state = initialState, action) => {
  switch (action.type) {
    case "createItem":
      let data = {
        id: Math.floor(Math.random() * 10000000000),
        barang: action.barang,
        harga: action.harga,
        photo: action.photo,
        pedagang: action.pedagang,
      };
      return (state = [...state, data]);

    case "updateItem":
      let indexUpdate = state.findIndex((value) => {
        return value.id === action.id;
      });

      const update = state;
      update.splice(indexUpdate, 1, {
        id: action.id,
        barang: action.barang,
        harga: action.harga,
        photo: action.photo,
        pedagang: action.pedagang,
      });
      return (state = [...update]);

    case "deleteItem":
      let indexDelete = state.findIndex((value) => {
        return value.id === action.id;
      });

      let index = state;
      index.splice(indexDelete, 1);
      return (state = [...index]);
    default:
      return state;
  }
};

export default item;
