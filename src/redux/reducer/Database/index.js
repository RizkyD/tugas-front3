const initialState = [
  {
    id: 1,
    username: "admin",
    password: "admin",
    role: "admin",
  },
  {
    id: 2,
    username: "pedagang",
    password: "pedagang",
    role: "pedagang",
  },
  {
    id: 3,
    username: "user",
    password: "user",
    role: "user",
  },
];

const database = (state = initialState, action) => {
  switch (action.type) {
    case "create":
      let data = {
        id: Math.floor(Math.random() * 10000000000),
        username: action.username,
        password: action.password,
        role: action.role,
      };
      return (state = [...state, data]);

    case "update":
      let indexUpdate = state.findIndex((value) => {
        return value.id === action.id;
      });

      const update = state;
      update.splice(indexUpdate, 1, {
        id: action.id,
        username: action.username,
        password: action.password,
        role: action.role,
      });
      return (state = [...update]);

    case "delete":
      let indexDelete = state.findIndex((value) => {
        return value.id === action.id;
      });

      let index = state;
      index.splice(indexDelete, 1);
      return (state = [...index]);
    default:
      return state;
  }
};

export default database;
