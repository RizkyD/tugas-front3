import { createStore } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web
import createCompressEncryptor from 'redux-persist-transform-compress-encrypt'


import allReducers from "../index";


const transformer = createCompressEncryptor({
  secretKey: 'secret-key',
  onError: function(error) {
      //fired whenever there is any issue with transformation, 
      //compression or encryption/decryption
  }
})

const persistConfig = {
  key: "root",
  transforms:[transformer],
  storage,
};

const persistedReducer = persistReducer(persistConfig, allReducers);

export default () => {
  let store = createStore(persistedReducer);
  let persistor = persistStore(store);
  return { store, persistor };
};
