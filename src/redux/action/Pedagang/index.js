const createItem = (barang, harga, photo, pedagang) => {
  return {
    type: "createItem",
    barang,
    harga,
    photo,
    pedagang,
  };
};

const updateItem = (id, barang, harga, photo, pedagang) => {
  return {
    type: "updateItem",
    id,
    barang,
    harga,
    photo,
    pedagang,
  };
};

const deleteItem = (id) => {
  return {
    type: "deleteItem",
    id,
  };
};

export { createItem, updateItem, deleteItem };
