const addCart = (user, id, barang, harga, photo) => {
  return {
    type: "addCart",
    id,
    barang,
    harga,
    photo,
    user,
  };
};

const deleteCart = (index) => {
  return {
    type: "deleteCart",
    index,
  };
};

export { addCart, deleteCart };
