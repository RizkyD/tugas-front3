const login = (id, username, password, role) => {
  return {
    type: "login",
    id,
    username,
    password,
    role,
  };
};

const logout = () => {
  return {
    type: "logout",
    status: false,
  };
};

export { login, logout };
