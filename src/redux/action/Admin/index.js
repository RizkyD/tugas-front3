const create = (username, password, role) => {
  return {
    type: "create",
    username,
    password,
    role,
  };
};

const update = (id, username, password, role) => {
  return {
    type: "update",
    id,
    username,
    password,
    role,
  };
};

const delet = (id) => {
  return {
    type: "delete",
    id,
  };
};

export { create, update, delet };
