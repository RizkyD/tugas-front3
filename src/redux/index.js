import { combineReducers } from "redux";
import auth from "./reducer/Auth";
import database from "./reducer/Database";
import item from "./reducer/Item";
import cart from "./reducer/Cart";

const allReducers = combineReducers({
  auth,
  database,
  item,
  cart,
});

export default allReducers;
